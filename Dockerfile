FROM keymetrics/pm2:latest-alpine

LABEL maintainer="Naphattharawat <naphattharawat@gmail.com>"

WORKDIR /home/test

RUN npm i npm@latest -g

RUN  apk add --no-cache --virtual deps \
    python

COPY ./ ./

RUN npm run build

# COPY ./server-script/ .

RUN npm i

RUN npm init -y && npm i express

COPY ./config/process.json .

CMD  ["pm2-runtime","start","process.json"]

EXPOSE 80